package com.manster.pay.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.manster.pay.entity.Product;

/**
 * @Author manster
 * @Date 2022/6/4
 **/
public interface ProductMapper extends BaseMapper<Product> {
}
