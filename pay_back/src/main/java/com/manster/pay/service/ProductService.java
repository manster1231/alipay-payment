package com.manster.pay.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.manster.pay.entity.Product;

/**
 * @Author manster
 * @Date 2022/6/4
 **/
public interface ProductService extends IService<Product> {
}
