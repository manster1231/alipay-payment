package com.manster.pay.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.manster.pay.entity.OrderInfo;
import com.manster.pay.entity.RefundInfo;
import com.manster.pay.mapper.RefundInfoMapper;
import com.manster.pay.service.OrderInfoService;
import com.manster.pay.service.RefundInfoService;
import com.manster.pay.util.OrderNoUtils;
import org.springframework.core.annotation.OrderUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @Author manster
 * @Date 2022/6/4
 **/
@Service
public class RefundInfoServiceImpl extends ServiceImpl<RefundInfoMapper, RefundInfo> implements RefundInfoService {

    @Resource
    private OrderInfoService orderInfoService;

    /**
     * 创建退款单
     * @param orderNo 订单号
     * @param reason 原因
     * @return
     */
    @Override
    public RefundInfo createRefundByOrderNoForAliPay(String orderNo, String reason) {
        //获取订单信息
        OrderInfo orderInfo = orderInfoService.getOrderByOrderNo(orderNo);
        //生成退款订单
        RefundInfo refundInfo = new RefundInfo();
        refundInfo.setOrderNo(orderNo);
        refundInfo.setRefundNo(OrderNoUtils.getRefundNo());//退款订单号
        refundInfo.setTotalFee(orderInfo.getTotalFee());//原订单金额
        refundInfo.setRefund(orderInfo.getTotalFee());//退款金额
        refundInfo.setReason(reason);

        baseMapper.insert(refundInfo);

        return refundInfo;
    }

    /**
     * 修改退款单状态
     * @param refundNo 退款单
     * @param content 退款响应
     * @param refundStatus 退款状态
     */
    @Override
    public void updateRefundForAliPay(String refundNo, String content, String refundStatus) {
        //根据退款单编号进行退款
        QueryWrapper<RefundInfo> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("refund_no", refundNo);

        //设置要修改的字段
        RefundInfo refundInfo = new RefundInfo();
        refundInfo.setRefundStatus(refundStatus);
        refundInfo.setContentReturn(content);

        //更新退款单
        baseMapper.update(refundInfo, queryWrapper);
    }
}
