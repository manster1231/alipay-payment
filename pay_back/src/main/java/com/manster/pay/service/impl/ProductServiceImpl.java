package com.manster.pay.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.manster.pay.entity.Product;
import com.manster.pay.mapper.ProductMapper;
import com.manster.pay.service.ProductService;
import org.springframework.stereotype.Service;

/**
 * @Author manster
 * @Date 2022/6/4
 **/
@Service
public class ProductServiceImpl extends ServiceImpl<ProductMapper, Product> implements ProductService {
}
