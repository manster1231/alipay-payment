package com.manster.pay.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.manster.pay.entity.OrderInfo;
import com.manster.pay.entity.Product;
import com.manster.pay.enums.OrderStatus;
import com.manster.pay.mapper.OrderInfoMapper;
import com.manster.pay.mapper.ProductMapper;
import com.manster.pay.service.OrderInfoService;
import com.manster.pay.util.OrderNoUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.time.Duration;
import java.time.Instant;
import java.util.List;

/**
 * @Author manster
 * @Date 2022/6/4
 **/
@Service
public class OrderInfoServiceImpl extends ServiceImpl<OrderInfoMapper, OrderInfo> implements OrderInfoService {

    @Resource
    private ProductMapper productMapper;

    //根据商品id创建订单
    @Override
    public OrderInfo createOrderByProductId(Long productId, String paymentType) {
        //查找已存在但未支付的订单
        OrderInfo orderInfo = this.getOne(new QueryWrapper<OrderInfo>()
                .eq("product_id", productId)
                .eq("order_status", OrderStatus.NOTPAY.getType())
                .eq("payment_type", paymentType)
        );
        if(orderInfo != null){
            return orderInfo;
        }

        //获取商品信息
        Product product = productMapper.selectById(productId);

        //生成订单
        orderInfo = new OrderInfo();
        orderInfo.setTitle(product.getTitle());
        orderInfo.setOrderNo(OrderNoUtils.getOrderNo());
        orderInfo.setProductId(productId);
        orderInfo.setTotalFee(product.getPrice());
        orderInfo.setOrderStatus(OrderStatus.NOTPAY.getType());
        orderInfo.setPaymentType(paymentType);

        baseMapper.insert(orderInfo);

        return orderInfo;
    }

    /**
     * 按创建时间倒序展示订单列表
     * @return
     */
    @Override
    public List<OrderInfo> listOrderByCreateTimeDesc() {
        QueryWrapper<OrderInfo> wrapper = new QueryWrapper<>();
        wrapper.orderByDesc("create_time");
        return baseMapper.selectList(wrapper);
    }

    /**
     * 根据订单号获取订单
     * @param outTradeNo
     * @return
     */
    @Override
    public OrderInfo getOrderByOrderNo(String outTradeNo) {
        OrderInfo orderInfo = baseMapper.selectOne(new QueryWrapper<OrderInfo>().eq("order_no", outTradeNo));
        return orderInfo;
    }

    /**
     * 更新订单状态
     * @param orderNo 订单号
     * @param orderStatus 订单状态
     */
    @Override
    public void updateStatusByOrderNo(String orderNo, OrderStatus orderStatus) {
        QueryWrapper<OrderInfo> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("order_no", orderNo);

        OrderInfo orderInfo = new OrderInfo();
        orderInfo.setOrderStatus(orderStatus.getType());

        baseMapper.update(orderInfo, queryWrapper);
    }

    /**
     * 查询订单状态
     * @param orderNo
     * @return
     */
    @Override
    public String getOrderStatus(String orderNo) {
        QueryWrapper<OrderInfo> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("order_no", orderNo);
        OrderInfo orderInfo = baseMapper.selectOne(queryWrapper);
        if (orderInfo == null) {
            return null;
        }
        return orderInfo.getOrderStatus();
    }

    /**
     * 查询超过minutes分钟并且未支付的订单
     * @param minutes 查询间隔时间
     * @param paymentType 支付类型
     * @return 返回未支付的订单
     */
    @Override
    public List<OrderInfo> getNoPayOrderByDuration(int minutes, String paymentType) {
        //当前时间减去minutes分钟的时间
        Instant instant = Instant.now().minus(Duration.ofMinutes(minutes));

        QueryWrapper<OrderInfo> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("order_status", OrderStatus.NOTPAY.getType());
        queryWrapper.eq("payment_type", paymentType);
        queryWrapper.le("create_time", instant);

        List<OrderInfo> orderInfoList = baseMapper.selectList(queryWrapper);

        return orderInfoList;
    }
}
