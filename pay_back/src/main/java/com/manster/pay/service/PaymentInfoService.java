package com.manster.pay.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.manster.pay.entity.PaymentInfo;

import java.util.Map;

/**
 * @Author manster
 * @Date 2022/6/4
 **/
public interface PaymentInfoService extends IService<PaymentInfo> {
    //记录支付日志
    void createPaymentInfoForAliPay(Map<String, String> params);
}
