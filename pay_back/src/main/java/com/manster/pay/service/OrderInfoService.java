package com.manster.pay.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.manster.pay.entity.OrderInfo;
import com.manster.pay.enums.OrderStatus;

import java.util.List;

/**
 * @Author manster
 * @Date 2022/6/4
 **/
public interface OrderInfoService extends IService<OrderInfo> {
    //根据商品id创建订单
    OrderInfo createOrderByProductId(Long productId, String paymentType);

    //按创建时间倒序展示订单列表
    List<OrderInfo> listOrderByCreateTimeDesc();

    //根据订单号获取订单
    OrderInfo getOrderByOrderNo(String outTradeNo);

    //更新订单状态
    void updateStatusByOrderNo(String orderNo, OrderStatus success);

    //查询订单状态
    String getOrderStatus(String orderNo);

    //查询超过minutes分钟并且未支付的订单
    List<OrderInfo> getNoPayOrderByDuration(int minutes, String paymentType);
}
