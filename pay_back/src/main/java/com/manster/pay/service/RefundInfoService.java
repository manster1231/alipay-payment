package com.manster.pay.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.manster.pay.entity.RefundInfo;

/**
 * @Author manster
 * @Date 2022/6/4
 **/
public interface RefundInfoService extends IService<RefundInfo> {
    //创建退款单
    RefundInfo createRefundByOrderNoForAliPay(String orderNo, String reason);

    //修改退款单
    void updateRefundForAliPay(String refundNo, String body, String type);
}
