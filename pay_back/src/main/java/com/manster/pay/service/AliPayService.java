package com.manster.pay.service;

import java.util.Map;

/**
 * @Author manster
 * @Date 2022/6/5
 **/
public interface AliPayService {
    //创建订单
    String tradeCreate(Long productId);

    //处理订单
    void processOrder(Map<String, String> params);

    //取消订单
    void cancelOrder(String orderNo);

    //查询订单
    String queryOrder(String orderNo);

    //检查订单状态
    void checkOrderStatus(String orderNo);

    //申请退款
    void refund(String orderNo, String reason);

    //查询退款
    String queryRefund(String orderNo);

    //获取账单url
    String queryBill(String billDate, String type);
}
