package com.manster.pay.task;

import com.manster.pay.entity.OrderInfo;
import com.manster.pay.enums.PayType;
import com.manster.pay.service.AliPayService;
import com.manster.pay.service.OrderInfoService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.List;

/**
 * @Author manster
 * @Date 2022/6/6
 **/
@Slf4j
@Component
public class AliPayTask {

    @Resource
    private OrderInfoService orderInfoService;

    @Resource
    private AliPayService aliPayService;

    /**
     * 从第0秒开始每隔30秒查询一次，查询创建超过5分钟并且未支付的订单
     */
    @Scheduled(cron = "0 0/5 * * * ?")
    public void orderConfirm() {

        log.info("========执行订单定时查询========");

        List<OrderInfo> orderInfoList = orderInfoService.getNoPayOrderByDuration(1, PayType.ALIPAY.getType());

        for (OrderInfo orderInfo : orderInfoList) {
            String orderNo = orderInfo.getOrderNo();
            log.warn("超时订单==> {}", orderNo);

            //核实订单状态，调用支付宝查单接口
            aliPayService.checkOrderStatus(orderNo);
        }
    }

}
