package com.manster.pay.controller;

import com.manster.pay.entity.Product;
import com.manster.pay.service.ProductService;
import com.manster.pay.util.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

/**
 * @Author manster
 * @Date 2022/6/3
 **/
@Api(tags = "商品管理")
@RestController
@RequestMapping("/api/product")
@CrossOrigin //跨域
public class ProductController {

    @Resource
    private ProductService productService;

    @ApiOperation("测试接口")
    @GetMapping("/test")
    public R test(){
        return R.ok().data("message", "hello").data("now", new Date());
    }

    @ApiOperation("获取商品列表")
    @GetMapping("/list")
    public R list(){
        List<Product> list = productService.list();
        return R.ok().data("productList", list);
    }

}
