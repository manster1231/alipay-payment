package com.manster.pay.enums.alipay;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @Author manster
 * @Date 2022/6/6
 **/
@AllArgsConstructor
@Getter
public enum AliPayTradeState {

    /**
     * 交易创建，等待买家付款
     */
    NOTPAY("WAIT_BUYER_PAY"),

    /**
     * 未付款交易超时关闭，或支付完成后全额退款
     */
    CLOSED("TRADE_CLOSED"),

    /**
     *  退款成功
     */
    REFUND_SUCCESS("REFUND_SUCCESS"),

    /**
     *  退款失败
     */
    REFUND_ERROR("REFUND_ERROR"),

    /**
     * 交易支付成功
     */
    SUCCESS("TRADE_SUCCESS");


    private final String type;

}
