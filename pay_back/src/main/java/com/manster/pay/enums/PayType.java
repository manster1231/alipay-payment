package com.manster.pay.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @Author manster
 * @Date 2022/6/5
 **/
@AllArgsConstructor
@Getter
public enum PayType {

    /**
     * 微信
     */
    WXPAY("微信"),

    /**
     * 支付宝
     */
    ALIPAY("支付宝");

    private final String type;

}
