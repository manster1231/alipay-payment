package com.manster.pay;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.env.Environment;

import javax.annotation.Resource;

/**
 * @Author manster
 * @Date 2022/6/4
 **/
@SpringBootTest
@Slf4j
public class AlipayTests {

    @Resource
    private Environment config;

    @Test
    public void testAlipayConfig(){
        log.info(config.getProperty("alipay.app-id"));
    }

}
