# Payment

基于 SpringBoot + Vue 的支付宝支付测试 demo 系统

可以查看博客来查看详情

[SpringBoot + Vue 结合支付宝支付（1）-- 准备工作](https://blog.csdn.net/qq_45803593/article/details/125152020)

[SpringBoot + Vue 结合支付宝支付（2）-- 项目搭建](https://blog.csdn.net/qq_45803593/article/details/125152161)

[SpringBoot + Vue 结合支付宝支付（3）--调用api](https://blog.csdn.net/qq_45803593/article/details/125152183)
