/*
SQLyog Ultimate v12.5.0 (64 bit)
MySQL - 8.0.22 : Database - payment_demo
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`payment_demo` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;

USE `payment_demo`;

/*Table structure for table `t_order_info` */

DROP TABLE IF EXISTS `t_order_info`;

CREATE TABLE `t_order_info` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT COMMENT '订单id',
  `title` varchar(256) DEFAULT NULL COMMENT '订单标题',
  `order_no` varchar(50) DEFAULT NULL COMMENT '商户订单编号',
  `user_id` bigint DEFAULT NULL COMMENT '用户id',
  `product_id` bigint DEFAULT NULL COMMENT '支付产品id',
  `total_fee` int DEFAULT NULL COMMENT '订单金额(分)',
  `code_url` varchar(50) DEFAULT NULL COMMENT '订单二维码连接',
  `order_status` varchar(10) DEFAULT NULL COMMENT '订单状态',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `payment_type` varchar(20) DEFAULT NULL COMMENT '支付方式',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

/*Data for the table `t_order_info` */

insert  into `t_order_info`(`id`,`title`,`order_no`,`user_id`,`product_id`,`total_fee`,`code_url`,`order_status`,`create_time`,`update_time`,`payment_type`) values 
(2,'Java课程','ORDER_20220605192436258',NULL,1,1,NULL,'支付成功','2022-06-05 19:24:36','2022-06-06 09:41:06','支付宝'),
(3,'Python课程','ORDER_20220605192609681',NULL,4,1,NULL,'用户已取消','2022-06-05 19:26:09','2022-06-06 10:11:36','支付宝'),
(4,'前端课程','ORDER_20220606101208832',NULL,3,1,NULL,'用户已取消','2022-06-06 10:12:08','2022-06-06 10:12:38','支付宝'),
(5,'大数据课程','ORDER_20220606104429667',NULL,2,1,NULL,'超时已关闭','2022-06-06 10:44:29','2022-06-06 11:14:01','支付宝'),
(6,'Java课程','ORDER_20220606111419406',NULL,1,1,NULL,'超时已关闭','2022-06-06 11:14:19','2022-06-06 11:15:31','支付宝'),
(7,'Python课程','ORDER_20220606112935103',NULL,4,1,NULL,'退款异常','2022-06-06 11:29:35','2022-06-06 16:13:05','支付宝'),
(8,'大数据课程','ORDER_20220606160937168',NULL,2,1,NULL,'退款异常','2022-06-06 16:09:37','2022-06-06 16:10:14','支付宝'),
(9,'大数据课程','ORDER_20220606161339109',NULL,2,1,NULL,'退款异常','2022-06-06 16:13:39','2022-06-06 16:15:51','支付宝'),
(10,'Python课程','ORDER_20220606161852499',NULL,4,1,NULL,'已退款','2022-06-06 16:18:52','2022-06-06 16:19:27','支付宝');

/*Table structure for table `t_payment_info` */

DROP TABLE IF EXISTS `t_payment_info`;

CREATE TABLE `t_payment_info` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT COMMENT '支付记录id',
  `order_no` varchar(50) DEFAULT NULL COMMENT '商户订单编号',
  `transaction_id` varchar(50) DEFAULT NULL COMMENT '支付系统交易编号',
  `payment_type` varchar(20) DEFAULT NULL COMMENT '支付类型',
  `trade_type` varchar(20) DEFAULT NULL COMMENT '交易类型',
  `trade_state` varchar(50) DEFAULT NULL COMMENT '交易状态',
  `payer_total` int DEFAULT NULL COMMENT '支付金额(分)',
  `content` text COMMENT '通知参数',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

/*Data for the table `t_payment_info` */

insert  into `t_payment_info`(`id`,`order_no`,`transaction_id`,`payment_type`,`trade_type`,`trade_state`,`payer_total`,`content`,`create_time`,`update_time`) values 
(1,'ORDER_20220605192436258','2022060522001467080501630856','支付宝','电脑网站支付','TRADE_SUCCESS',1,'{\"gmt_create\":\"2022-06-05 19:24:41\",\"charset\":\"UTF-8\",\"gmt_payment\":\"2022-06-05 19:24:50\",\"notify_time\":\"2022-06-05 19:24:52\",\"subject\":\"Java程\",\"buyer_id\":\"2088622959267080\",\"invoice_amount\":\"0.01\",\"version\":\"1.0\",\"notify_id\":\"2022060500222192451067080519987290\",\"fund_bill_list\":\"[{\\\"amount\\\":\\\"0.01\\\",\\\"fundChannel\\\":\\\"ALIPAYACCOUNT\\\"}]\",\"notify_type\":\"trade_status_sync\",\"out_trade_no\":\"ORDER_20220605192436258\",\"total_amount\":\"0.01\",\"trade_status\":\"TRADE_SUCCESS\",\"trade_no\":\"2022060522001467080501630856\",\"auth_app_id\":\"2021000120606577\",\"receipt_amount\":\"0.01\",\"point_amount\":\"0.00\",\"app_id\":\"2021000120606577\",\"buyer_pay_amount\":\"0.01\",\"seller_id\":\"2088621959288381\"}','2022-06-05 19:24:53','2022-06-05 19:24:53'),
(2,'ORDER_20220606112935103','2022060622001467080501631000','支付宝','电脑网站支付','TRADE_SUCCESS',1,'{\"code\":\"10000\",\"msg\":\"Success\",\"buyer_logon_id\":\"jyb***@sandbox.com\",\"buyer_pay_amount\":\"0.00\",\"buyer_user_id\":\"2088622959267080\",\"buyer_user_type\":\"PRIVATE\",\"invoice_amount\":\"0.00\",\"out_trade_no\":\"ORDER_20220606112935103\",\"point_amount\":\"0.00\",\"receipt_amount\":\"0.00\",\"send_pay_date\":\"2022-06-06 11:29:53\",\"total_amount\":\"0.01\",\"trade_no\":\"2022060622001467080501631000\",\"trade_status\":\"TRADE_SUCCESS\"}','2022-06-06 11:31:00','2022-06-06 11:31:00'),
(3,'ORDER_20220606160937168','2022060622001467080501631003','支付宝','电脑网站支付','TRADE_SUCCESS',1,'{\"gmt_create\":\"2022-06-06 16:09:45\",\"charset\":\"UTF-8\",\"gmt_payment\":\"2022-06-06 16:09:52\",\"notify_time\":\"2022-06-06 16:09:54\",\"subject\":\"大数据课程\",\"buyer_id\":\"2088622959267080\",\"invoice_amount\":\"0.01\",\"version\":\"1.0\",\"notify_id\":\"2022060600222160953067080519989803\",\"fund_bill_list\":\"[{\\\"amount\\\":\\\"0.01\\\",\\\"fundChannel\\\":\\\"ALIPAYACCOUNT\\\"}]\",\"notify_type\":\"trade_status_sync\",\"out_trade_no\":\"ORDER_20220606160937168\",\"total_amount\":\"0.01\",\"trade_status\":\"TRADE_SUCCESS\",\"trade_no\":\"2022060622001467080501631003\",\"auth_app_id\":\"2021000120606577\",\"receipt_amount\":\"0.01\",\"point_amount\":\"0.00\",\"app_id\":\"2021000120606577\",\"buyer_pay_amount\":\"0.01\",\"seller_id\":\"2088621959288381\"}','2022-06-06 16:09:55','2022-06-06 16:09:55'),
(4,'ORDER_20220606161339109','2022060622001467080501631097','支付宝','电脑网站支付','TRADE_SUCCESS',1,'{\"gmt_create\":\"2022-06-06 16:13:45\",\"charset\":\"UTF-8\",\"gmt_payment\":\"2022-06-06 16:13:54\",\"notify_time\":\"2022-06-06 16:13:55\",\"subject\":\"大数据课程\",\"buyer_id\":\"2088622959267080\",\"invoice_amount\":\"0.01\",\"version\":\"1.0\",\"notify_id\":\"2022060600222161355067080519992901\",\"fund_bill_list\":\"[{\\\"amount\\\":\\\"0.01\\\",\\\"fundChannel\\\":\\\"ALIPAYACCOUNT\\\"}]\",\"notify_type\":\"trade_status_sync\",\"out_trade_no\":\"ORDER_20220606161339109\",\"total_amount\":\"0.01\",\"trade_status\":\"TRADE_SUCCESS\",\"trade_no\":\"2022060622001467080501631097\",\"auth_app_id\":\"2021000120606577\",\"receipt_amount\":\"0.01\",\"point_amount\":\"0.00\",\"app_id\":\"2021000120606577\",\"buyer_pay_amount\":\"0.01\",\"seller_id\":\"2088621959288381\"}','2022-06-06 16:13:56','2022-06-06 16:13:56'),
(5,'ORDER_20220606161852499','2022060622001467080501631004','支付宝','电脑网站支付','TRADE_SUCCESS',1,'{\"gmt_create\":\"2022-06-06 16:18:57\",\"charset\":\"UTF-8\",\"gmt_payment\":\"2022-06-06 16:19:04\",\"notify_time\":\"2022-06-06 16:19:05\",\"subject\":\"Python课程\",\"buyer_id\":\"2088622959267080\",\"invoice_amount\":\"0.01\",\"version\":\"1.0\",\"notify_id\":\"2022060600222161904067080519989804\",\"fund_bill_list\":\"[{\\\"amount\\\":\\\"0.01\\\",\\\"fundChannel\\\":\\\"ALIPAYACCOUNT\\\"}]\",\"notify_type\":\"trade_status_sync\",\"out_trade_no\":\"ORDER_20220606161852499\",\"total_amount\":\"0.01\",\"trade_status\":\"TRADE_SUCCESS\",\"trade_no\":\"2022060622001467080501631004\",\"auth_app_id\":\"2021000120606577\",\"receipt_amount\":\"0.01\",\"point_amount\":\"0.00\",\"app_id\":\"2021000120606577\",\"buyer_pay_amount\":\"0.01\",\"seller_id\":\"2088621959288381\"}','2022-06-06 16:19:05','2022-06-06 16:19:05');

/*Table structure for table `t_product` */

DROP TABLE IF EXISTS `t_product`;

CREATE TABLE `t_product` (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '商Bid',
  `title` varchar(20) DEFAULT NULL COMMENT '商品名称',
  `price` int DEFAULT NULL COMMENT '价格(分)',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

/*Data for the table `t_product` */

insert  into `t_product`(`id`,`title`,`price`,`create_time`,`update_time`) values 
(1,'Java课程',1,'2022-06-03 17:44:41','2022-06-05 19:25:37'),
(2,'大数据课程',1,'2022-06-03 17:44:41','2022-06-03 17:44:41'),
(3,'前端课程',1,'2022-06-03 17:44:41','2022-06-05 19:25:41'),
(4,'Python课程',1,'2022-06-03 17:44:41','2022-06-05 19:25:52');

/*Table structure for table `t_refund_info` */

DROP TABLE IF EXISTS `t_refund_info`;

CREATE TABLE `t_refund_info` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT COMMENT '款单id',
  `order_no` varchar(50) DEFAULT NULL COMMENT '商户订单编号',
  `refund_no` varchar(50) DEFAULT NULL COMMENT '商户退款单编号',
  `refund_id` varchar(50) DEFAULT NULL COMMENT '支付系统退款单号',
  `total_fee` int DEFAULT NULL COMMENT '原订单金额(分)',
  `refund` int DEFAULT NULL COMMENT '退款金额(分)',
  `reason` varchar(50) DEFAULT NULL COMMENT '退款原因',
  `refund_status` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '退款状态',
  `content_return` text COMMENT '申请退款返回参数',
  `content_notify` text COMMENT '退款结果通知参数',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

/*Data for the table `t_refund_info` */

insert  into `t_refund_info`(`id`,`order_no`,`refund_no`,`refund_id`,`total_fee`,`refund`,`reason`,`refund_status`,`content_return`,`content_notify`,`create_time`,`update_time`) values 
(1,'ORDER_20220606160937168','REFUND_20220606161013221',NULL,1,1,'买错了','REFUND_ERROR','{\"alipay_trade_refund_response\":{\"code\":\"40004\",\"msg\":\"Business Failed\",\"sub_code\":\"ACQ.TRADE_NOT_EXIST\",\"sub_msg\":\"交易不存在\",\"refund_fee\":\"0.00\",\"send_back_fee\":\"0.00\"},\"sign\":\"bpKFaGv9vQQ9WnI3lOzLOgEdn+R4YGaVPADnYhnySg2t4+/tjAp4KjYmQ8lElxX3YoZg+BYnplyk72y2ikXfWLtOCm76yIGk9NqdqSIuntnI3fTVarFSXbZSVAVenTIQk5yUx2VKlZlYNlmc3SOFo33mKbHLgWGFk5IJXWIlOLXRuS35YDgk02l7odS2HY3TmHsSxmX0lanmnhzgZ47Juyh72hr6Hkwj002nAJXEipEXnHtEnVWXMZsWH9nf/kWH2OrUF5a1TxYouX4mujMUPQx5iCAJ9UBtoJ9vNYQYx7mMP4IibjfbbA+mkjwYgMw4jJg57+YZ4hz/SxTaZvvVyw==\"}',NULL,'2022-06-06 16:10:13','2022-06-06 16:10:14'),
(2,'ORDER_20220606112935103','REFUND_20220606161304624',NULL,1,1,'不喜欢','REFUND_ERROR','{\"alipay_trade_refund_response\":{\"code\":\"40004\",\"msg\":\"Business Failed\",\"sub_code\":\"ACQ.TRADE_NOT_EXIST\",\"sub_msg\":\"交易不存在\",\"refund_fee\":\"0.00\",\"send_back_fee\":\"0.00\"},\"sign\":\"bpKFaGv9vQQ9WnI3lOzLOgEdn+R4YGaVPADnYhnySg2t4+/tjAp4KjYmQ8lElxX3YoZg+BYnplyk72y2ikXfWLtOCm76yIGk9NqdqSIuntnI3fTVarFSXbZSVAVenTIQk5yUx2VKlZlYNlmc3SOFo33mKbHLgWGFk5IJXWIlOLXRuS35YDgk02l7odS2HY3TmHsSxmX0lanmnhzgZ47Juyh72hr6Hkwj002nAJXEipEXnHtEnVWXMZsWH9nf/kWH2OrUF5a1TxYouX4mujMUPQx5iCAJ9UBtoJ9vNYQYx7mMP4IibjfbbA+mkjwYgMw4jJg57+YZ4hz/SxTaZvvVyw==\"}',NULL,'2022-06-06 16:13:04','2022-06-06 16:13:05'),
(3,'ORDER_20220606161339109','REFUND_20220606161551931',NULL,1,1,'不喜欢','REFUND_ERROR','{\"alipay_trade_refund_response\":{\"code\":\"40004\",\"msg\":\"Business Failed\",\"sub_code\":\"ACQ.TRADE_NOT_EXIST\",\"sub_msg\":\"交易不存在\",\"refund_fee\":\"0.00\",\"send_back_fee\":\"0.00\"},\"sign\":\"bpKFaGv9vQQ9WnI3lOzLOgEdn+R4YGaVPADnYhnySg2t4+/tjAp4KjYmQ8lElxX3YoZg+BYnplyk72y2ikXfWLtOCm76yIGk9NqdqSIuntnI3fTVarFSXbZSVAVenTIQk5yUx2VKlZlYNlmc3SOFo33mKbHLgWGFk5IJXWIlOLXRuS35YDgk02l7odS2HY3TmHsSxmX0lanmnhzgZ47Juyh72hr6Hkwj002nAJXEipEXnHtEnVWXMZsWH9nf/kWH2OrUF5a1TxYouX4mujMUPQx5iCAJ9UBtoJ9vNYQYx7mMP4IibjfbbA+mkjwYgMw4jJg57+YZ4hz/SxTaZvvVyw==\"}',NULL,'2022-06-06 16:15:51','2022-06-06 16:15:51'),
(4,'ORDER_20220606161852499','REFUND_20220606161925958',NULL,1,1,'不喜欢','REFUND_SUCCESS','{\"alipay_trade_refund_response\":{\"code\":\"10000\",\"msg\":\"Success\",\"buyer_logon_id\":\"jyb***@sandbox.com\",\"buyer_user_id\":\"2088622959267080\",\"fund_change\":\"Y\",\"gmt_refund_pay\":\"2022-06-06 16:19:26\",\"out_trade_no\":\"ORDER_20220606161852499\",\"refund_fee\":\"0.01\",\"send_back_fee\":\"0.00\",\"trade_no\":\"2022060622001467080501631004\"},\"sign\":\"gYvzdXVE4B9ifIAdEFQaeJu3ZhP8Bxetb2cuZ+7F75eC85R3NJicKtkga0x80QI9lWLFpkDkBoH9vP4bpdxKIPSizJ8vbrXnk11mKRnHCT00XOw/l61/KgoYWuRLsMGKZge5mdyNNtezFaVX+v1JDFNwO+SSCj9cSQXeMIGe6YxaxQkX+ykqT98bF42H33r5q+qD+adumIXl2WPRPSA6W0AgfEW99pLv60jd+mAEF9C0pgWp5+8bBYBMt17Cbz0ruQUTSjnvwSt2JD5TAxdD5MK+HBqFKVZyQ3l5LTl5mwmAehrKn1wm9iRvB9vHddU14bxjTp4FTlaKlXal5BQDmQ==\"}',NULL,'2022-06-06 16:19:25','2022-06-06 16:19:27');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
